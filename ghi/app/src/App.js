import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Nav from './Nav';
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import ConferenceForm from './Conference';
import AttendForm from './AttendConference';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm /> } />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm /> } />
          </Route>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={props.attendees}/> } />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendForm /> } />
          </Route>
          <Route path="presentation">
            <Route path="new" element={<PresentationForm /> } />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
